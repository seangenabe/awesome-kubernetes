## Other lists

* [ramitsurana's list](https://github.com/ramitsurana/awesome-kubernetes)
* [tmrts' list](https://github.com/tmrts/awesome-kubernetes)

## Charts

### API gateway

* [Kong](https://konghq.com/)
* [Traefik](https://traefik.io/)
* [Tyk](https://tyk.io/)

### Monitoring

* [Grafana](https://grafana.com/)
* [Prometheus](https://prometheus.io/)

### Security

* [cert-manager](https://hub.kubeapps.com/charts/stable/cert-manager) - automate the management and issuance of TLS certificates from various issuing sources. Supercedes kube-lego.
  * ~~kube-lego~~

### Service mesh

Talk: What is a service mesh? https://youtu.be/sJVV0GFYz7k

* [Linkerd](https://linkerd.io/) - Linkerd is an open source network proxy designed to be deployed as a service mesh: a dedicated layer for managing, controlling, and monitoring service-to- service communication within an application.
* [Istio](https://istio.io/)
* [Cilium](https://cilium.io/)

### Service proxy

* [Envoy](https://www.envoyproxy.io/)
* [nginx](https://www.nginx.com/)
* [HAProxy](http://www.haproxy.org/)

### Software development

* [GitLab](https://docs.gitlab.com/ee/install/kubernetes/) - Complete DevOps. GitLab is a web-based Git-repository manager with wiki and issue-tracking features

## Helpers

### Deployment

* [Draft](https://draft.sh/) - streamlined Kubernetes development
* [kubectx / kubens](https://github.com/ahmetb/kubectx) - switch between Kubernetes clusters and namespaces
